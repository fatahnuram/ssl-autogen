#!/usr/bin/env bash

# variable declarations
DIR_NAME="docker-cert"

# check args num
if [[ $# -gt 1 ]]; then
    # exit if more than 1 arg
    echo
    echo "Too many arguments! Aborting.."
    echo
    exit 1
fi

# check if any argument exist
if [[ ! -z ${1} ]]; then
    # save dir name
    DIR_NAME=${1}
fi

# integrate with cleanup script
sed -i s/DIR_NAME="\(.*\)"/DIR_NAME=\"${DIR_NAME}\"/g cleanup.sh

# prompt user
echo
echo "Packing files into ${DIR_NAME} directory.."
echo

# check file existence
if $(ls *.srl &> /dev/null) || $(ls *.pem &> /dev/null) || $(ls *.cnf &> /dev/null); then
    # create new directory
    mkdir -p ${DIR_NAME}
    # clean directory if already exist
    rm -rf ${DIR_NAME}/*
else
    # prompt user and force exit
    echo
    echo "No files to move.."
    echo
    exit 0
fi

# move all certificate things to created folder
if ls *.srl &> /dev/null; then
    mv -vf *.srl ${DIR_NAME}
fi
if ls *.pem &> /dev/null; then
    mv -vf *.pem ${DIR_NAME}
fi
if ls *.cnf &> /dev/null; then
    mv -vf *.cnf ${DIR_NAME}
fi

# prompt user
echo
echo "Done!"
echo "It's better to move the ${DIR_NAME} directory to somewhere safe, like maybe, in your /opt/${DIR_NAME}"
echo
