# ssl-autogen

Bash script for generating self-signed SSL certificate in use for Docker remote daemon execution

## Notes

This repository is now **working**. However, this repository is still **in development phase**.

## How to run

Make sure the script executable

```bash
chmod +x *.sh
```

Configure script to match your needs

```bash
./configure.sh
```

Export variables from script

```bash
source cert.conf
```

Finally, run the script

```bash
./generate.sh
```

Optional: you can pack all generated files into a directory named `docker-cert` (default) by running script below

```bash
./pack.sh
```

Or maybe you want to pack them into `somedir` directory simply by running

```bash
./pack.sh somedir
```

Optional: you can remove all generated files by running script below

```bash
./cleanup.sh
```

## Limitations

Some limitations for this project until now:

- Data verification and validation is not implemented yet
- Script only generate SSL certificate, but not changing Docker server configuration (so you need to change them yourself)
- etc..

## LICENSE

![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png "WTFPL")

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

Copyright (C) 2018 Fatah Nur Alam Majid <fatahnuram@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
