#!/usr/bin/env bash

# check args num
if [[ $# -gt 0 ]]; then
    # exit if more than 0 arg
    echo
    echo "Too many arguments! Aborting.."
    echo
    exit 1
fi

# variable declarations
# ask certificate password
printf "New certificate password: "
read -s PASSWD
# general
DOMAIN="${CERT_DOMAIN}"
# file naming
CONF_SERVER="server-extfile.cnf"
CONF_CLIENT="client-extfile.cnf"
# certificate request details
C="${CERT_C}" # country
ST="${CERT_ST}" # state
L="${CERT_L}" # locality
O="${CERT_O}" # organization
OU="${CERT_OU}" # organizational unit, or project name
CN="${CERT_DOMAIN}" # common name
EMAIL="${CERT_EMAIL}" # email address

# predefined function
prompt_user() {
  echo
  echo "$@"
  echo
}

# get public ip
prompt_user "Getting public IP address.."
PUBLIC_IP=$(curl -s ipinfo.io/ip)
# handling if no internet connection
if [[ -z ${PUBLIC_IP} ]]; then
  echo "Connection failed! Check your internet connection.."
  exit 1
fi
# prompt user if success getting public IP address to connect
prompt_user "Got public IP address: ${PUBLIC_IP}"

# clear existing cert things
prompt_user "Removing existing files.."
export CERT_AUTO_MODE="yes"
chmod +x cleanup.sh
. cleanup.sh
unset CERT_AUTO_MODE

# create extra conf file
prompt_user "Generating extra conf file.."
echo "subjectAltName = DNS:${CN},IP:${PUBLIC_IP}" > ${CONF_SERVER}
echo "extendedKeyUsage = serverAuth" >> ${CONF_SERVER}
echo "extendedKeyUsage = clientAuth" > ${CONF_CLIENT}

# create certificate
prompt_user "Creating CA certificates.."
openssl genrsa -aes256 -passout pass:"${PASSWD}" -out ca-key.pem 4096
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca-cert.pem -passin pass:"${PASSWD}" \
-subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/OU=${OU}/CN=${CN}/emailAddress=${EMAIL}"
prompt_user "Creating server certificates.."
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=${CN}" -sha256 -new -key server-key.pem -out server.csr
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca-cert.pem -CAkey ca-key.pem \
-CAcreateserial -passin pass:"${PASSWD}" -out server-cert.pem -extfile "${CONF_SERVER}"
prompt_user "Creating client certificates.."
openssl genrsa -out client-key.pem 4096
openssl req -subj "/CN=client" -new -key client-key.pem -out client.csr
openssl x509 -req -days 365 -sha256 -in client.csr -CA ca-cert.pem -CAkey ca-key.pem \
-CAcreateserial -passin pass:"${PASSWD}" -out client-cert.pem -extfile "${CONF_CLIENT}"

# change permission
prompt_user "Changing permissions.."
chmod -v 400 *key.pem
chmod -v 444 *cert.pem

# cleanup
prompt_user "Cleaning up.."
rm -vf *.csr
# enable certificate for docker remote daemon in CI/CD environment
mv -vf ca-cert.pem ca.pem
mv -vf client-cert.pem cert.pem
mv -vf client-key.pem key.pem

# prompt user about how to use this cert
echo
echo "Done!"
echo
echo "Download files below to connect to this Docker daemon remotely:"
echo "ca.pem, cert.pem, key.pem"
echo
echo "Or by running script below from your local machine:"
echo "scp ${USER}@${PUBLIC_IP}:${PWD}/{ca,cert,key}.pem ."
echo
echo "And then you can access this Docker daemon remotely by running:"
echo "docker --tlsverify --tlscacert=ca.pem --tlscert=cert.pem --tlskey=key.pem -H=${PUBLIC_IP}:2375 info"
echo
