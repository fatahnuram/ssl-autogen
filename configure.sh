#!/usr/bin/env bash

# check args num
if [[ $# -gt 0 ]]; then
    # exit if more than 0 arg
    echo
    echo "Too many arguments! Aborting.."
    echo
    exit 1
fi

# general variables
printf "Server URL: "
read DOMAIN

# certificate request details
printf "Country [AU,US,ID,etc]: "
read C
printf "State or province: "
read ST
printf "Locality: "
read L
printf "Organization: "
read O
printf "Organizational Unit: "
read OU
printf "Email address: "
read EMAIL

# export all variables
echo "export CERT_DOMAIN=${DOMAIN}" > cert.conf
echo "export CERT_C=${C}" >> cert.conf
echo "export CERT_ST=${ST}" >> cert.conf
echo "export CERT_L=${L}" >> cert.conf
echo "export CERT_O=${O}" >> cert.conf
echo "export CERT_OU=${OU}" >> cert.conf
echo "export CERT_EMAIL=${EMAIL}" >> cert.conf

# prompt user when done
echo
echo "Configuration saved! You can use them by running:"
echo "source cert.conf"
echo
echo "And then run the script to generate:"
echo "./generate.sh"
echo
